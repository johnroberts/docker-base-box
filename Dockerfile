FROM alpine:latest

# Core tools
RUN apk add --update --no-cache zsh git bash curl tmux \
    python2 python3

# ohmyzsh setup 
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
RUN rm -f ~/.zshrc
RUN curl -sSL https://gitlab.com/snippets/1917234/raw | `which zsh`

# launch zsh
CMD /bin/zsh
